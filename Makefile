.PHONY: docs clean

COMMAND = docker-compose run --rm web /bin/sh -c

all: clean build test

build:
	docker-compose build

run:
	docker-compose up

migrate:
	$(COMMAND) "cd webapp && ./manage.py migrate ${OPTIONS}"

showmigrations:
	$(COMMAND) "cd webapp && ./manage.py showmigrations"

django-manage:
	$(COMMAND) "cd webapp && ./manage.py ${CMD}"

django-shell:
	$(COMMAND) "cd webapp; ./manage.py shell"

test:
	$(COMMAND) "cd webapp && ./manage.py test ${OPTIONS} ${MODULE_NAME}"

clean:
	rm -rf build
	rm -rf hello.egg-info
	rm -rf dist
	rm -rf htmlcov
	rm -rf .tox
	rm -rf .cache
	rm -rf .pytest_cache
	find . -type f -name "*.pyc" -delete
	rm -rf $(find . -type d -name __pycache__)
