from rest_framework import serializers

from users.models import User

# Serializers define the API representation.
class UserSerializer(serializers.Serializer):
    id          = serializers.IntegerField(label='ID', read_only=True)
    name        = serializers.CharField(help_text='user name', max_length=256)
    dob         = serializers.DateField(allow_null=True,
                    help_text='date of birth', required=False)
    address     = serializers.CharField(allow_blank=True, allow_null=True,
                    help_text='user address', required=False, style={'base_template': 'textarea.html'})
    description = serializers.CharField(allow_blank=True, allow_null=True,
                    help_text='user description', required=False, style={'base_template': 'textarea.html'})
    created_at  = serializers.DateTimeField(help_text='user created date',
                    read_only=True)

    def validate_name(self, value):
        """
        Validation rules for name field.
        There are two cases where we need to perform this.

        1. When request has been made to create a new user.
        2. When request has been made to update the user
           with the updated value for the name field.
           In case of update, if the update value is different
           then only do the validation! :)
        """

        # should validate check for both cases.
        should_validate = (
            (self.instance is None) or
            (self.instance is not None and self.instance.name.lower() != value.lower())
        )

        if should_validate and User.objects.filter(name=value).exists():
            raise serializers.ValidationError("Duplicate user exists with the same name")

        return value

    def create(self, validated_data):
        """
        Create and return a new `User` instance, given the validated data.
        """
        return User.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `User` instance, given the validated data.
        """
        instance.name = validated_data.get('name', instance.name)
        instance.dob = validated_data.get('dob', instance.dob)
        instance.address = validated_data.get('address', instance.address)
        instance.description = validated_data.get('description', instance.description)
        instance.save()
        return instance
