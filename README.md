# ryde-users-REST

REST APIs for user resource.

## Service Dependencies:
- [Django](https://www.djangoproject.com/): web framework in python
- [Django REST Framework](https://www.django-rest-framework.org/): for the REST API development
- [Django YASG](https://drf-yasg.readthedocs.io/en/stable/readme.html): API document generator (swagger)

## Notes:
- This app uses django's ORM feature and SQLite has been used to store the data (file based db) for the sake of the demo.
- Please make sure, docker and docker-compose are installed on your system with the latest version. Docker is used to containerise the service and docker-compose to manage the service.

## Steps to invoke the serivce:

### 1. using only virtualenv
1. Clone the repo: `git clone https://gitlab.com/rsudip90/ryde-users-rest.git`
2. Navigate to project dir: `cd ryde-users-rest`
2. Set up virtualenv `venv` under the root of the project using `virtualenv venv --py python3`
3. Activate the virtualenv using `source venv/bin/activate`
4. Install the python dependencies using `pip install -r requirements.txt`
5. Navigate to main app dir: `cd webapp`
5. Run the migration using `python manage.py migrate`
6. Start the server using `python manage.py runserver 0.0.0.0:8000` and try to access http://localhost:8000/users/
7. You can access the API documentation at http://localhost:8000/swagger/ location, where you can try some of the APIs.
8. To execute the test cases, just use `./manage.py test`
9. To deactivate the virtual environment, use `deactivate`

### 2. using docker and docker-compose
1. Clone the repo: `git clone https://gitlab.com/rsudip90/ryde-users-rest.git`
2. Navigate to project dir: `cd ryde-users-rest`
3. Build the image using `docker-compose build` or just use makefile utility `make build`
4. Apply migration using `make migrate`
5. Run the service using `make run` and try to access http://localhost:8000/users/
6. You can access the API documentation at http://localhost:8000/swagger/ location, where you can try some of the APIs.
7. To execute the test cases, just use `make test`

## Thoughts on Advanced requirements
- Provide a complete logging (when/how/etc.) strategy.
> We can use structlog with some informative log message. It will automatically wrap the timestamp, level, ip, etc...

- Related to the requirement above, suppose the address of user now includes a geographic coordinate(i.e., latitude and longitude), can you build an API that
> Yes, we can use google map API to do this in the background using celery by passing the received requested lat, long.

- given a user name
> We can use one of the libraries which can provide some random value for user names.

For the rest of the items under Advanced requirements, didn't spend much time. :)
