from django.db import models

class User(models.Model):
    name            = models.CharField(max_length=256, unique=True, help_text="user name")
    dob             = models.DateField(blank=True, null=True, help_text="date of birth")
    address         = models.TextField(blank=True, null=True, help_text="user address")
    description     = models.TextField(blank=True, null=True, help_text="user description")
    created_at      = models.DateTimeField(auto_now_add=True, editable=False, help_text="user created date")
    # modified_at     = models.DateTimeField(auto_now=True, editable=False, help_text="user modified date")

    class Meta:
        db_table = "user"
        app_label = "users"

    def __str__(self):
        return f"(pk: {self.pk}, name: {self.name})"
