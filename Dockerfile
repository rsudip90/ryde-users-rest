FROM python:3.8

ENV PYTHONUNBUFFERED=1

WORKDIR /code

COPY requirements.txt /code/

RUN apt-get update && \
    pip install --upgrade pip && \
    pip install -r requirements.txt --no-cache-dir

COPY . /code/

CMD ["python", "webapp/manage.py", "runserver", "0.0.0.0:8000"]
