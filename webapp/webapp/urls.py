from django.urls import path
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

from users import views

# API documentation view
schema_view = get_schema_view(
   openapi.Info(
      title="APIs",
      default_version="v1",
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

# url paths
urlpatterns = [
    path('users/', views.user_list, name='user-list'),
    path('users/<int:pk>', views.user_detail, name='user-detail'),
    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
]
