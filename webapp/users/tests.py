from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from users.models import User
from users.serializers import UserSerializer


class ListUsersTest(APITestCase):
    """
    Test case for listing users.
    """

    def setUp(self):
        User.objects.create(name="john", dob="1970-01-01")
        User.objects.create(name="jake", dob="2000-01-01")

    def test_list_users(self):
        """
        Ensure we can get the list users as per data we have in the DB.
        """
        url = reverse('user-list')
        response = self.client.get(url, format="json")

        # get data from db
        users = User.objects.all()
        serializer = UserSerializer(users, many=True)

        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class CreateUserTest(APITestCase):
    """
    Test case for creating a new user.
    """

    def setUp(self):
        self.payload = {
            "name": "john doe",
            "dob": "1970-01-01",
            "address": "somewhere, earth",
            "description": "How you doing?",
        }

    def test_create_user(self):
        """
        Ensure we can create a new user object.
        """
        url = reverse('user-list')
        response = self.client.post(url, self.payload, format="json")

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(User.objects.get().name, 'john doe')


class GetUserTest(APITestCase):
    """
    Test case for getting a user.
    """

    def setUp(self):
        self.john = User.objects.create(name="john", dob="1970-01-01")
        self.jake = User.objects.create(name="jake", dob="2000-01-01")

    def test_get_valid_user(self):
        """
        Ensure we can get the specific user details as per data we have in the DB.
        """
        url = reverse('user-detail', kwargs={"pk": self.john.pk})
        response = self.client.get(url, format="json")

        # get data from db
        john = User.objects.get(pk=self.john.pk)
        serializer = UserSerializer(john)

        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_user(self):
        """
        Ensure we can get 404 for invalid user pk.
        """

        url = reverse('user-detail', kwargs={"pk": 999})
        response = self.client.get(url, format="json")

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class UpdateUserTest(APITestCase):
    """
    Test case for updating a user.
    """

    def setUp(self):
        self.john = User.objects.create(name="john", dob="1970-01-01")
        self.payload = {
            "name": "john doe",
            "dob": "1990-01-01",
            "description": "Hi, how are you?",
            "address": "lost, earth",
        }

    def test_update_valid_user(self):
        """
        Ensure we can update the specific user details as per data we have in the DB.
        """
        url = reverse('user-detail', kwargs={"pk": self.john.pk})
        response = self.client.put(url, self.payload, format="json")

        # get data from db
        john = User.objects.get(pk=self.john.pk)
        serializer = UserSerializer(john)

        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_invalid_user(self):
        """
        Ensure we can get 404 for invalid user pk.
        """

        url = reverse('user-detail', kwargs={"pk": 999})
        response = self.client.get(url, format="json")

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class DeleteUserTest(APITestCase):
    """
    Test case for deleting a user.
    """

    def setUp(self):
        self.john = User.objects.create(name="john", dob="1970-01-01")

    def test_delete_user(self):
        """
        Ensure we can delete the specific user.
        """
        url = reverse('user-detail', kwargs={"pk": self.john.pk})
        response = self.client.delete(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        # get data from db
        john = User.objects.filter(pk=self.john.pk).first()
        self.assertEqual(john, None)
